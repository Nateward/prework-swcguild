function luckySevens() {

	// Starting bet
	var pot = (document.getElementById("betAmount").value);

	var winnings = 0;

	var	winCount = 0;

	var minMsg = ("You must enter at least $1!!!");
	
	var potWinnings = 0;

	var rolls = 0;

	var wins = 0;

	var won = true;

	var test = false;

	if (pot <= 0) {
		test = false;
		document.getElementById("amountAlert").innerHTML = minMsg;
		return alert(minMsg);
	}
	if (isNaN(pot)) {
		test = false;
		document.getElementById("amountAlert").innerHTML = minMsg;
		return alert(minMsg);
    }
	else {
		test = true;
		document.getElementById("startBet").innerHTML = "$" + (Number(pot)).toFixed(2);
	}
	while (pot > 0){
		var	roll1 = Math.floor(Math.random() * 6) + 1;
		var roll2 = Math.floor(Math.random() * 6) + 1;
		var rollTotal = roll1 + roll2;

		// If statement for winnings and lossings
		if (rollTotal == 7){
			pot += 4;
			potWinnings += 4;
			rolls += 1;
			wins += 1;
			won = true;
		}
		else{
			pot -= 1;
			potWinnings = 0;
			rolls += 1
			wins = 0
			won = false;


		}

		if (potWinnings > winnings && won == true){
			winnings = winnings + (potWinnings - winnings);
			winCount = wins;
		}
	}

	if(pot <= 0 && test == true){
		document.getElementById("rolls").innerHTML = (Number(rolls));
		document.getElementById("winnings").innerHTML = "$" + (Number(winnings)).toFixed(2);
		document.getElementById("winCount").innerHTML = (Number(winCount));
		document.getElementById("playButton").innerHTML = "PLAY AGAIN!!!";

	}

}
